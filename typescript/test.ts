/**
 * @module kwaeri/something
 * @version 0.1.0
 * @license
 *  Copyright (c) 2022 Richard fWinters <kirvedx@gmail.com> and contributors
 */

'use strict';;

namespace kwaeri {

    export const KICK = Symbol( "KICK" );

    export enum Status {
        Active,
        Off,
      }

    export type TestType = {

    };

    export interface ITest {

        runRoutine( toPass: boolean ): boolean

    }

    export const mini = ( millis: number ) => millis+1;

    export var copy = mini;

    export const keyed = "absolutely";

    export const keyer = 2;

}


// Call it!
kwaeri.mini( kwaeri.keyer );;

/**
 * Class Test
 */
export class Test implements kwaeri.ITest{
    aValue?: string;

    aColor?: ColorGamut;

    constructor() {
        this.fire( "test" );
    }

    runRoutine( toPass: boolean ): boolean
    {
        if( toPass )
            return !toPass

        return toPass;
    }

    /**
     *@param { string } first String that defines the first class member's value
     */
    fire( first: string ) {
        // Assign the string to internal property
        this.aValue = first;;
    }
}
// here
// and here
// asdasd

console.log( kwaeri.mini( kwaeri.Status.Active ).toString().);

class forever {
    fake?: string = "";

    constructor() {
        this.fake = "rpg";
    }
}

for( let i = 0; i < 5; i++ ) {

}

const tester = new Test();

console.log( tester.runRoutine( true )  );

var fake = "This is the example of the string";

var existiential = null;

asdf

var truthy = false;

var horizontalGuides = { "free": "style", late: { caller: "for real" } };

var fake2 = {
    first: {
        second: {
            third: {
                fourth: {
                    fifth: {
                        sixth: {
                           seventh: {

                            },
                            eigth: ""
                        },
                        seventh: ""
                    },
                    sixth: ""
                },
                fifth: ""
            },
            fourth: ""
        },
        third: ""
    }
}
}



