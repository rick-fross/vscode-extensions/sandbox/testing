# NOTES

These are notes inside of a markdown file

## Part I

Here we do a bunch of example markdown, like how we  `let everything = "inline"`..

---

<details>
<summary>
Show extras:
</summary>

- Or, in fact, the headings above (the top level heading 1 "Notes", or this heading 2 "Part I")

  They are made using `# ` characters, 1 for each level, so a single one for heading 1, and 6 for heading 6.

- You can alternatively use `===` below text to make it a heading 1 like so (it wont get highlighted in the editor though.):

  Didn't realize how it worked
  ===

- Or use `---` to make a Heading 2 like so (again, it wont get highlighted in the editor - make sure there's text above it or it becomes a horizontal rule):

  Didn't realize how this worked either
  ----

</details>

---

### Unordered Lists

Here's a list made with [asterisks][1] (*), various nesting:

---

<details>
<summary>
Show the example:
</summary>

* Base item, with an `inlined raw`
* Base item 2
    * Nested item 1 (2 spaces in)
      * Nested nested item #2 (2 more spaces in)
        * 4th level
* Base item.3
    * Nested item 3 (4 spaces in)
      * Nested item 4 (2 more spaces in)
* Base item 4
  * Nested item 5 (2 spaces in)
  > Nested code block
      * Nested item 6 (4 more spaces in)
* Base item 5
    * Nested item 6 (4 spaces in)
        * Nested item 7 (4 more spaces in)
        ```
        // With raw fenced code block (6 spaces in, each line)
        int i = 0;
        ```
</details>

---

### Unordered List Alternatives

We can make unordered [lists](https://www.markdownguide.org/basic-syntax/#lists-1 "Markdown Lists Syntax") several [^footnote-1] ways


#### Hyphens

<details>
<summary>
Show the hyphens example:
</summary>
<br />

An unordered list made with [hyphens][2] (-):

- Here
    - It is
- With varied
    - Levels of nesting
        - Again
</details>

#### Plus Signs

And one made with [plus signs][3] (+):

<details>
<summary>
Show the plus signs example:
</summary>
<br />

+ Here
  + It
    + Is
  * Done
      * Right (with 4 spaces, renders list properly but markup is not highlighted when only 2 spaces used on 2nd indent)

</details>

#### Combination

One made with a [combination][4]?:

<details>
<summary>
Show the hyphens example:
</summary>
<br />

* Here (*)
  + it (+)
    - Is (-)
  + Done
      - Right (again, with 4 spaces, renders list properly with 2 - but fails to highlight markdown without 4 spaces on 2nd indent level)

</details>
<br />

---

### Ordered List

Here's an [ordered][5] list made with all ones (1.):

<details>
<summary>
Show the ordered list example:
</summary>
<br />

1. Ordered item 1
   1. Nested item 1 (3 spaces in, 2 doesnt work - perhaps due to period?)
1. Ordered item 2
    1. Nested item 2 (4 spaces in)
       1. Double-Nested item 1 (3 more spaces in)
   1. Nested item 3 (3 spaces in)
      1. Double-Nested item 2 (3 more spaces in)
         1. Triple-Nested item 1 (3 more spaces in from Double-Nested item 2)
       1. Double-Nested item 3 (4 more spaces in from Nested item 3)
         1. Double-Nested item 4 (3 more spaces in from Nested item 3)
            1. Triple-Nested item 2 (3 more spaces in from Double-Nested item 4)
            ```typescript
            export const fake = {
              really: true,
              reallyReally: false,
              count: 5
            }
            ```
1. Ordered item 3
    1. Nested item 4 (4 spaces in)
        1. Double-Nested item 5 (4 more spaces in)
            1. Triple Nested item 3 (4 more spaces in from Nested item 4)
                1. Quadruple Nested item 4 ( 4 more spaces in)
                * When doing an ordered list
                * Stick with
                  * 4 spaces (this was done with 2 :P)
                    * For consistency (2, again)
                    .
                    > Stick with 4 spaces for consistency
                    >
                    > > For consistency!
                    > > > Esepcially!
                    > > > > For
                    > > > > > Real
                    > > > > > > Dude
                    > > > > > > > Seriously?
                    > > > > > > > > Yep
                    > > > > > > > > > We
                    > > > > > > > > > > can
                    > > > > > > > > > > > keep
                    > > > > > > > > > > > > Going
                    > > > > > > > > > > > > > Now
</details>

***

### Tasks {task-lists}

Here's a task list (part of [extended syntax]()), which does not work in vscode:


<details>
<summary>
Show the task list example:
</summary>
<br />

+ [x] First task
+ [ ] Second Task
- [ ] Third Task

</details>

### Definition Lists

Definition lists don't work in vscode.

<details>
<summary>
Show the definition lists example:
</summary>
<br />

Here is a term
 Here is the definition

</details>

### Math (KaTex)

Math can be shown inline with `$...$`, like so: $a^2+b/c=d^5$

<details>
<summary>Show math examples:</summary>
You can also use math with back-ticks inside of the dollar signs like so: $`a+b+c=d`$

Math can be done in a fenced codeblock with the language *math* specified:

```Math
a+b+c=d
```

You can show math written inline on a separate line using double dollar signs (`$$...$$`), like so: $$a^2+b^2=c^2$$

Though, you can also use dollar signs like fences on a separate set of lines to accomplish the same:

$$
a^2 + b^2 = c^2
$$
</details>

### Diffs

Do not work:

* {+ Addition +}
* {- Deleted -}

### Mermaid Graphs

Are not supported:

<details>
<summary>See proof in example</summary>

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```
</details>

### Code Blocks

A [raw][6] code block:


<details>
<summary>
Show the code block example:
</summary>
<br />

  ```javascript
  // With highlighted fenced code block
  var second = "Testing";
```

How about [highlighted][7] code block?

```typescript
// A comment
var item = "Testing, 1, 2, 3.";

const meth = ( item ) => console.log( item );

// Call the method:
meth( "A make believe string" );
```
</details>

---

### Inline HTML

How about some inline markup, like a line break `<br />` <br /> before a block quote:

> Here's a block quote for ya!

---

### Block Quotes

A nested [blockquote][8] :

<details>
<summary>
Show the block quote example:
</summary>
<br />

> Here's a multi-line block quote
>
> > With a nested block quote
> >
> > > And yet another nested block quote
>
> and other goodies:
>
> - Like
> - This
> - List
>
> 1. This
> 1. Numbered
> 1. List
>
> Or *this* or _that_ stylized __sparkling__ **goodness** with ***NO*** *__NO__* __*unintended*__ ~~mistakes~~!

</details>
<br />

A fenced block quote:

<details>
<summary>
Show the fenced block quote example:
</summary>

>>>
A multiline blockquote

that takes place within a block quote  fence!

Amazing, right?
>>>

</details>

___

### Table

And a [table][9] full of the majority of our styling and emphasis markup/markdown:

<details>
<summary>
Show the table and styling example:
</summary>
<br />

| Style                       | key             | Example                           |
|:---                         | :----           | :----:                            |
| Bold                        | * (x2)          | **boldened text**                      |
| Bold                        | _ (x2)          | __boldened text__                      |
| Italic                      | _ (x1)          | _italicized text_.                      |
| Strikethrough               | ~ (x2)          | ~~strike through text~~                 |
| Highlight                   | = (x2)          | ==highlighted text==                    |
| Highlight                   |\<mark>\</mark>  | <mark>highlighted text</mark>            |
| Subscript                   | ~               | h~2~0                                   |
| Subscript                   | \<sub>\</sub>   | h<sub>2</sub>0                          |
| Superscript                 | ^               | x^5^                                     |
| Superscript                 | \<sup>\</sup>   | x<sup>5</sup>                            |
| Bold & Italic               | * (x3)          |  ***bold and italic***                  |
| Bold & Italic               | _ (x2) & <br /> * (x1) |  *__bold and italic__*                  |
| Bold & Italic               | _ (x1) & <br /> * (x2) |  **_bold and italic_**                  |
| Bold, Italic, Strikethrough | ~ (x2) & <br/> * (x3) | ~~***bold italic and strikethrough***~~ |
| Bold, Italic, Strikethrough | ~ (x2) & <br/> * (x1) & <br /> _ (x2) | ~~*__bold italic and strikethrough__*~~ |
| Bold, Italic, Strikethrough | ~ (x2) & <br/> * (x2) & <br /> _ (x1) | ~~**_bold italic and strikethrough_**~~ |
| Keyboard Glyphs             | \<kbd>KeyText\</kbd>  | <kbd>CTRL</kbd> + <kbd>SHIFT</kbd> + <kbd>Enter</kbd> |

</details>

### Links

How about links:

<details>
<summary>
Show the links example(s):
</summary>
<br />

A clever [link](https://google.com "A slight description")

An image link:

[![Dickybits Logo](../images/dickybits-retro-gradient-block-logo.png "The new and imporved dickybits logo")]("https://www.mmod.co")

</details>
<br />

## References

_**Foot Notes**_ are where we specify the descriptions and link references for our inline references - they are a form of link that allows for simple notation within bodies of text so as not to overly convolute it, keeping it cleaner, by allowing us to specify addresses, descriptions, later in the file.

Below we have the foot note references specified, but they are not rendered in any preview like the anchors for the reference, such as [this one][10], made using `[this one][10]`, that will point to `## References` as defined below (which you cannot see):

[1]: #unordered-lists "Unordered List"
[2]: #hyphens 'Unordered List Alternatives: Hyphens'
[3]: #plus-signs (Unordered List Alternatives: Plus SIgns)
[4]: #combination "Unordered List Alternatives: Combination"
[5]: #ordered-list 'Ordered List'
[6]: #code-blocks (Raw Code Blocks)
[7]: #code-blocks "Highlighted Code Blocks"
[8]: #block-quotes 'Block Quotes'
[9]: #tables (Tables)
[10]: #references "References"


[^footnote-1]: We have several choices options for markup that denotes unordered lists; Asterisks (*), Hyphens (-), and Plus Signs (+).


