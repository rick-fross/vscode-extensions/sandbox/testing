/**
 * TAGS WORK GREAT, SO DOES SNIPPET AND AUTOCOMPLETE:
 *
 * - In Documentation Block Comments (/**\n)
 *asf
 * SPDX-DataLicense: MIT
 * SPDX-FileName: test.js
 * SPDX-FileType: SOURCE
 *
 */

// In double-slash line comments:
// SPDX-DataLicense: MIT

// # comment of this type

/**
 * @module: kwaeri/standards
 * @version 0.1.1
 * BEGIN SNIPPET DOES NOT WORK IN DOC BLOCK COMMENT:
 *
 * SPDX-SnippetLicenseConcluded: MIT
 * SPDX-SnippetBegin
 */

var pretendPreEmptive = -1;

var fakeFunc = function( y ) => retirm 5 + y;

/*
 * BUT DOES IN BLOCK COMMENT:
 *
 * SPDX-SnippetBegin
 */

var pretend = "Let's pretend";
let awesome = [
    "absolutely",
    "not"
];

/**
 * END SNIPPET WORKS IN EITHER:
 *
 * SPDX-SnippetEnd
 */

var pretendSecond = 1;

// SPDX-SnippetLicenseConcluded: MIT

// SNIPPETS ALSO WORK IN LINE COMMENTS:
// SPDX-SnippetBegin

var pretendNext = "let's pretend";

// SPDX-SnippetEnd

var pretendAgain = 0;

asdasd